"""
this is so much easier
damn!!!
"""
import logging
import json
import random
import requests
from flask import Flask, request

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

# for telegram
TOKEN = ''
URL = "https://api.telegram.org/bot{}/".format(TOKEN)

# answer file
ANSWERS = "answers.json"

#  need some stuff to store the ids
CHAT_IDS = "chat_ids.json"

# load the text ansers
with open(ANSWERS) as f:
    json_file = json.load(f)


def do_stuff():
    """this is some gonna be our telegram bot"""
    # get last chat
    chat = get_last_chat_id(get_updates())
    text = "super text!"
    send_message(text, chat)
    print("I do stuff")

#def random_text(dryness):
#    text_list = text_answers["dryness"][dryness]["text"]
#    return random.choice(text_list)
#
#def random_gif(dryness):
#    gif = text_answers["dryness"][dryness]["gif"]
#    return random.choice(gif)
#
#def random_audio(dryness):
#    text_list = text_answers["dryness"][dryness]["audio"]
#    return random.choice(text_list)

def get_random_answer(dryness):
    answer_list = json_file["dryness"][dryness]
    return random.choice(answer_list)


def send_spam(dryness, spam_rate):
    chat = get_last_chat_id(get_updates())
    for _ in range(spam_rate):
        #text = random_text(dryness)
        answer = get_random_answer(dryness)
        send_message(answer, chat)
    #send_audio("CQADAgADtgEAAkBKiEoIiC0qk2-BSQI", chat)

def spam(humidity):
    """ decide on what to send"""
    h = float(humidity)
    if (h<0.0):
        return
    elif (h>=0.0 and h < 25.0):
        #vvdry()
        dryness = "really really dry"
        spam_rate = 7
    elif (h>=25.0 and  h < 37.5):
        #vdry()
        dryness = "really dry"
        spam_rate = 4
    elif (h>=37.5 and h<50):
        #dry()
        dryness = "dry"
        spam_rate = 1
    else:
        dryness = "wet"
        spam_rate = 0
        pass
        #wet()
    send_spam(dryness, spam_rate)
    print(dryness)

def  handle_humidity(humidity):
    chat = get_last_chat_id(get_updates())
    text = humidity
    send_message(text, chat)
   # print("I do stuff")


def get_url(url):
    response = requests.get(url)
    content = response.content.decode("utf8")
    return content


def get_json_from_url(url):
    content = get_url(url)
    js = json.loads(content)
    return js


def get_updates():
    url = URL + "getUpdates"
    js = get_json_from_url(url)
    return js

def dumpasfuck(updates):
    num_updates = len(updates["result"])
    last_update = num_updates - 1
    text = updates["result"][last_update]
    #chat_id = updates["result"][last_update]["message"]["chat"]["id"]
    print(text)

def get_last_chat_id_and_text(updates):
    num_updates = len(updates["result"])
    last_update = num_updates - 1
    text = updates["result"][last_update]["message"]["text"]
    chat_id = updates["result"][last_update]["message"]["chat"]["id"]
    return (text, chat_id)

def get_last_chat_id(updates):
    num_updates = len(updates["result"])
    last_update = num_updates - 1
    #text = updates["result"][last_update]["message"]["text"]
    chat_id = updates["result"][last_update]["message"]["chat"]["id"]
    return chat_id


# what to send
def send_message(answer, chat_id):
    if(answer["type"] == "text"):
        send_text_message(answer["value"], chat_id)
    if(answer["type"] == "gif"):
        send_gif(answer["value"], chat_id)
    if(answer["type"] == "audio"):
        send_audio(answer["value"], chat_id)

def send_text_message(text, chat_id):
    url = URL + "sendMessage?text={}&chat_id={}".format(text, chat_id)
    get_url(url)

def send_gif(gif, chat_id):
    url = URL + "sendDocument?document={}&chat_id={}".format(gif, chat_id)
    #print(url)
    get_url(url)

def send_audio(audio, chat_id):
    url = URL + "sendAudio?audio={}&chat_id={}".format(audio, chat_id)
    #print(url)
    get_url(url)


#dumpasfuck(get_updates())

app = Flask(__name__)
# act on this POST thing request
@app.route('/', methods=['POST'])
def result():
    print('Got a POST request.\n')
    #print(request.form['foo'])
    # here we can get the humidity
    # right now it is only the foo thing
    #if(request.form['foo'] == 'bar'):
    if(request.form['humidity']):
        # lets call our telegram bot
        #handle_humidity(request.form['humidity'])
        spam(request.form['humidity'])
    return 'Received!'
